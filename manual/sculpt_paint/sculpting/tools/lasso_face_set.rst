
**************
Lasso Face Set
**************

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Lasso Face Set`

Creates a new :doc:`Face Set </sculpt_paint/sculpting/editing/face_sets>`
based on a :ref:`lasso selection <tool-select-lasso>`.


Tool Settings
=============

Front Faces Only
   Only creates a mask on the faces that face towards the view.
